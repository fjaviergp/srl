"""

srl.py This class implement the Safe Reinforcement Learning algorithm. Depending on the risk function you use, it implemenents the
PI-SRL algorithm (step risk function) or the PR-SRL (continuous risk function)

"""

import random
import numpy as np

from cb import CB
from case import Case
from ounoise import OUNoise

class SRL:

	''' Initialization of the algorithm '''
	def __init__(self, cb, risk, baseline, alpha = 0.25, gamma = 0.9, Theta = 100, mu = 0, theta = 0.01, sigma = 0.001):
		self.cb = cb
		self.risk = risk
		self.theta = self.risk.theta
		self.baseline = baseline
		self.alpha = alpha
		self.gamma = gamma
		self.expNoise = OUNoise(self.cb.getDimAction(), mu=mu, theta=theta, sigma=sigma)
		self.Theta = Theta
		self.totalRwEpisode = 0
		self.maxTotalRwEpisode = -9999999
		self.listCasesEpisode = []
		self.listRewards = []

	''' Case generation '''
	def step( self, state, reward ):
		action = ''
		ncase, dist = self.cb.getNearestCase(state)
		cnew = ''
		rand = random.uniform(0, 1)
		if rand < (1 - self.risk.getRisk(dist)):
			# Known state
			action = ncase.getAction()
			noise = self.expNoise.noise()
		        action = np.array(action) + noise
			action = action.tolist()
			cnew = Case(0, ncase.getState(), action, ncase.getValue())
		else:
			# Unknown state
			action = self.baseline.getAction(state)
			cnew = Case(0, state, action, -1)
		self.totalRwEpisode = self.totalRwEpisode + reward
		self.listRewards.append(reward)
		self.listCasesEpisode.append(cnew)

		return action

	''' Computing the state-value function for the unknown states '''
	def computeStateValueUnknown( self, discount ):
		for i in range(len(self.listCasesEpisode)):
			c = self.listCasesEpisode[i]
			if c.getValue() == -1: 
				# Unknown state
				gamma = 1
				if self.firstVisit(c.getState(), i) and i < (len(self.listCasesEpisode) - 1):
					# First visit of the state in the episode
					acumReward = 0
					k = i+1
					for j in range(k, len(self.listCasesEpisode)):
						if not discount:
							acumReward = acumReward + self.listRewards[j]
						else:
							acumReward = acumReward + gamma * self.listRewards[j]
							gamma = gamma * self.gamma
					c.setValue(acumReward)


	''' Return true if the visit to the state is the first visit in the episode '''
	def firstVisit( self, state, index ):
		first = True
		for i in range(index):
			if self.equalArrays(state, self.listCasesEpisode[i].getState()):
				first = False
				return first
		return first

	''' Return true if two arrays are equal '''
	def equalArrays( self, state1, state2 ):
		eq = True
		for i in range(len(state1)):
			if state1[i] != state2[i]:
				eq = False
				return eq
		return eq

	
	''' Updating the cases in B using the experience gathered '''
	def updateCases( self ):
		if self.totalRwEpisode > (self.maxTotalRwEpisode - self.Theta):
			self.maxTotalRwEpisode = max(self.totalRwEpisode, self.maxTotalRwEpisode)
			for i in range(len(self.listCasesEpisode)):
				c = self.listCasesEpisode[i]
				ncase, dist = self.cb.getNearestCase(c.getState())
				if dist < self.theta:
					# Known state
					if i < (len(self.listCasesEpisode) - 1):
						delta = self.listRewards[i+1] + self.gamma * self.listCasesEpisode[i+1].getValue() - ncase.getValue()
						if delta > 0:
							ncase.setAction(c.getAction())
							ncase.setValue(ncase.getValue() + self.alpha * delta)
				else:
					#Unknown state
					self.cb.add(c)
		self.cb.removeLeastFrequentlyCases()
		self.expNoise.reset() 
		self.listCasesEpisode = []

	''' Save the case base '''
	def save( self ):
		self.cb.save()

	def printCasesEpisode( self ):
		print("\nCases in the episode: ")
		for i in range(len(self.listCasesEpisode)):
			c = self.listCasesEpisode[i]
			print(c.toString())
		print()


if __name__ == '__main__':
	"""
	The main function called when cbrimitation.py is run
	from the command line:

	> python srl.py

	"""
	from baselineheli import BaselineHeli
	from steprisk import StepRisk

	cb = CB(2, 1)
	cb.load()	
	cb.printCases()

	baseline = BaselineHeli()
	risk = StepRisk(0.3)

	srl = SRL(cb, risk, baseline)
	srl.step([2, 3], 2)
	srl.step([1, 2], 4)
	srl.step([4, 5], 3)
	srl.step([3, 7], 4)	
	srl.printCasesEpisode()

	srl.computeStateValueUnknown(False)
	srl.printCasesEpisode()
	srl.updateCases()
	cb.printCases()
