"""

steprisk.py holds the logic of the step risk function of algorithm PI-SRL

"""

from risk import Risk

class StepRisk(Risk):
	def __init__(self, theta):
		self.theta = theta
	
	def getRisk(self, dist):
		if dist > self.theta:
			return 1
		else:
			return 0
        	
