"""

mccbr.py Monte Carlo algorithm adapted to CBR

"""

from cb import CB
from case import Case

class MCCBR:

	''' Initialization of the MonteCarlo algorithm '''
	def __init__(self, cb, gamma = 0.9):
		self.cb = cb
		self.gamma = gamma
		self.listEpisode = []

	''' Return the action of the given state '''
	def getAction( self, tupleSRA ):
		s = tupleSRA[0]
		ncase, dist = self.cb.getNearestCase(s)
		tupleSRA.append(ncase.getAction())
		self.listEpisode.append(tupleSRA)
		return ncase.getAction()

	''' First Visit Monte Carlo algorithm '''
	def monteCarloFV( self, discount = True):
		for i in range(len(self.listEpisode)):
			s = self.listEpisode[i][0]
			ncase, dist = self.cb.getNearestCase(s)
			gamma = 1
			if self.firstVisit(ncase.getState(), i) and i < (len(self.listEpisode) - 1):
				acumReward = 0
				k = i+1
				for j in range(k, len(self.listEpisode)):
					if not discount:
						acumReward = acumReward + self.listEpisode[j][1]
					else:
						acumReward = acumReward + gamma * self.listEpisode[j][1]
						gamma = gamma * self.gamma
				ncase.setValue(acumReward)
		self.listEpisode = []

	''' Return true if the visit to the state is the first visit in the episode '''
	def firstVisit( self, state, index ):
		first = True
		for i in range(index):
			if self.equalArrays(state, self.listEpisode[i][0]):
				first = False
				return first
		return first

	''' Return true if two arrays are equal '''
	def equalArrays( self, state1, state2 ):
		eq = True
		for i in range(len(state1)):
			if state1[i] != state2[i]:
				eq = False
				return eq
		return eq

	''' Save the case base '''
	def save( self ):
		self.cb.save()

	''' Print the tuples (state, action, reward) in the episode '''
	def printListEpisode( self ):
		print("\nTuples episode:")
		for i in range(len(self.listEpisode)):
			if len(self.listEpisode[i]) == 3:
				print("State: ", self.listEpisode[i][0], " Reward: ", self.listEpisode[i][1], " Action: ", self.listEpisode[i][2])
			else:
				print("State: ", self.listEpisode[i][0], " Action: ", self.listEpisode[i][1])
		print()

if __name__ == '__main__':
	"""
	The main function called when cbrimitation.py is run
	from the command line:

	> python mccbr.py

	"""
	cb = CB(2, 1)
	cb.load()	
	cb.printCases()

	mc = MCCBR(cb)
	mc.getAction([[1,2]])	# 1st state in the episode
	mc.getAction([[3,4],3]) # 2nd state and reward of first transition
	mc.getAction([[5,3],4]) # 3rd state and reward of second transition
	mc.printListEpisode()
	mc.monteCarloFV()
	cb.printCases()
	

	pass

	
		

	
