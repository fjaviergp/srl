"""

cbrimitation.py holds the logic of the imitation of behaviors

"""

from cb import CB
from baseline import Baseline
from case import Case

class CBRImitation:

	''' Initialization of the algorithm '''
	def __init__( self, dimState, dimAction, baseline, risk, maxSize = 200, kd = True, win = 1, episodesWin = 1):
		self.cb = CB(dimState, dimAction, maxSize, kd)
		self.baseline = baseline
		self.risk = risk
		self.win = win
		self.episodesWin = 1
		self.newCases = []

	''' Imitation of the baseline behavior '''
	def imitate( self, state ):
		action = ''
		if self.cb.size() == 0:
			# The case base is empty
			action = self.baseline.getAction(state)
			case = Case(self.cb.getAdditions(), state, action, 0)
			self.cb.add(case)
		else:
			# Normal functioning
			ncase, dist = self.cb.getNearestCase(state)
			if self.risk.getRisk(dist) == 0:
				action = ncase.getAction()
				self.cb.increaseVisitTime(ncase)
			else:
				action = self.baseline.getAction(state)
				case = Case(self.cb.getAdditions(), state, action, 0)
				if self.win != -1:
					self.newCases.append(case)
					if self.episodesWin >= self.win:
						self.addCases()
						self.episodesWin = 0
				else:
					self.cb.add(case)

			self.cb.removeLeastFrequentlyCases()

		self.episodesWin = self.episodesWin + 1
		return action

	''' Add the cases in newCases to the case base '''
	def addCases( self ):
		for i in range(len(self.newCases)):
			case = self.newCases[i]
			self.cb.add(case)
		self.newCases = []

	''' Save the case base '''
	def save( self ):
		self.cb.save()

	''' Return the size of the case base '''
	def size( self ):
		return self.cb.size()

	''' Print the cases '''
	def printCases( self ):
		self.cb.printCases()

if __name__ == '__main__':
	"""
	The main function called when cbrimitation.py is run
	from the command line:

	> python cbrimitation.py

	"""
	from baselineheli import BaselineHeli
	from steprisk import StepRisk

	baseline = BaselineHeli()
	risk = StepRisk(0.3)
	cbr = CBRImitation(2, 3, baseline, risk)
	cbr.imitate([2, 3])
	cbr.imitate([1, 5])
	cbr.imitate([1, 5])
	cbr.imitate([3, 9])
	cbr.printCases()

	pass

	
		

	

