"""

case.py holds the logic of a single case

"""

class Case:

	''' Initialization of the case '''
	def __init__( self, idc, state, action, value):
		self.idc = idc
		self.state = state
		self.action = action
		self.value = value
		self.lastVisit = 0

	''' Set the state of the case '''
	def setState( self, state ):
		self.state = state

	''' Set the action of the case '''
	def setAction( self, action ):
		self.action = action

	''' Set the value of the case '''
	def setValue( self, value ):
		self.value = value

	''' Set the time of the last visit '''
	def setLastVisit( self, visit):
		self.lastVisit = visit

	''' Return the time of the last visit '''
	def getLastVisit( self ):
		return self.lastVisit

	''' Return the id '''
	def getId( self ):
		return self.idc

	''' Return the state '''
	def getState( self ):
		return self.state

	''' Return the action '''
	def getAction( self ):
		return self.action

	''' Return the value '''
	def getValue( self ):
		return self.value

	''' Return a string with the case information '''
	def toString( self ):
		return "ID: ", self.idc, " state: ", self.state, " action: ", self.action, " value: ", self.value, " last visit: ", self.lastVisit
