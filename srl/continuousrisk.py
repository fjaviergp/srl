"""

continuousrisk.py holds the logic of the step risk function of algorithm PI-SRL

"""

import numpy as np

from risk import Risk


class ContinuousRisk(Risk):
	def __init__(self, k, theta):
		self.k = k
		self.theta = theta
	
	def getRisk(self, dist):
		r = 1 - 1/(1+np.exp((self.k/self.theta)*((dist - self.theta/self.k)-self.theta)))
		return r
        	
