"""

cb.py holds the logic of the Case Base

"""

import math
import pickle
from case import Case
import numpy as np
try:
	from kdquery import Tree
except:
	pass

class CB:

	''' Initialization of the case base '''
	def __init__( self, dimState, dimAction, maxSize = 200, kd = True):
		self.cases = []
		self.dimState = dimState
		self.dimAction = dimAction
		self.maxSize = maxSize
		self.kd = kd
		if self.kd:
			self.tree = Tree(k=dimState,capacity=self.maxSize*10)
		self.additions = 0

	''' Add a new case in the case base '''
	def add( self, case):
		self.cases.append(case)
		if self.kd:
			self.tree.insert(case.getState(), [case.getAction(), case.getValue()])
		self.additions = self.additions + 1

	''' Increase the time of the last visit of the cases in the case base '''
	def increaseVisitTime( self, case ):
		for i in range(self.size()):
			case = self.getCase(i)
			case.setLastVisit(case.getLastVisit() + 1)
		case.setLastVisit(0)

	''' Return the dimension of the states '''	
	def getDimState( self ):
		return self.dimState

	''' Return the number of additions to the case base '''		
	def getAdditions( self ):
		return self.additions

	''' Return the dimension of the actions '''	
	def getDimAction( self ):
		return self.dimAction

	''' Return the max size of the case base '''	
	def getMaxSize( self ):
		return self.maxSize

	''' Return if it is using kd tree '''	
	def getIsKD( self ):
		return self.kd

	''' Return the nearest case to the given state '''
	def getNearestCase( self, state):
		if self.kd:
			node_id, dist = self.tree.find_nearest_point(state)
		else:
			dist = -1
			node_id = -1
			for i in range(self.size()):
				c = self.getCase(i)
				d = self.euclidean(state, c.getState())
				if dist == -1 or d < dist:
					dist = d
					node_id = i
		return self.cases[node_id], dist

	''' Return the euclidean distance between two states '''
	def euclidean( self, state1, state2):
		s = 0
		for i in range(len(state1)):
			s = s + math.pow(state1[i] - state2[i], 2)
		return math.sqrt(s)

	''' Return the case in position index in the case base '''
	def getCase( self, index ):
		return self.cases[index]

	''' Remove a given case from the case base '''
	def remove( self, case):
		cs, pos = self.equal(case)
		if pos != -1:
			self.cases.remove(cs)
			if self.kd:
				self.tree.deactivate(case.getId())	

	''' Return the case in B that is the same that the case passed as argument '''	
	def equal(self, case ):
		cs = ''
		pos = -1
		for i in range(self.size()):
			c = self.getCase(i)
			if self.equalArrays(case.getState(), c.getState()) and self.equalArrays(case.getAction(), c.getAction()):
				cs = c
				pos = i
				return cs, pos
		return cs, pos

	''' Return true if two arrays are equal '''
	def equalArrays( self, state1, state2 ):
		eq = True
		for i in range(len(state1)):
			if state1[i] != state2[i]:
				eq = False
				return eq
		return eq
		

	''' Remove the least frequently used cases in the case base '''	
	def removeLeastFrequentlyCases( self ):
		maxIndex = []
		if self.size() > self.maxSize:
			numRemoveCases = self.size() - self.maxSize
			for i in range(self.size()):
				case = self.getCase(i)
				lastVisit = case.getLastVisit()
				pos = self.getIndexMin(maxIndex)
				if len(maxIndex) < numRemoveCases:
					maxIndex.append(i)
				elif pos != -1 and self.getCase(i).getLastVisit() > self.getCase(maxIndex[pos]).getLastVisit():
					maxIndex[pos] = i

			for i in range(len(maxIndex)):
				case = self.getCase(maxIndex[i])
				self.remove(case)
				if self.kd:
					self.tree.deactivate(case.getId())	

	''' Return the position of the min value of listv '''	
	def getIndexMin(self, listv):
		posMin = -1
		minValue = -1
		for i in range(len(listv)):
			if minValue == -1 or self.getCase(listv[i]).getLastVisit() < minValue:
				minValue = self.getCase(listv[i]).getLastVisit()
				posMin = i
		return posMin

	''' Return the size of the case base '''
	def size( self ):
		return len(self.cases)

	''' Save the case base '''
	def save( self ):
		pickle_out = open("cases.pickle","wb")
		pickle.dump(self.cases, pickle_out)
		pickle_out.close()
		
	''' Load the case base '''
	def load( self ):
		pickle_in = open("cases.pickle","rb")
		self.cases = pickle.load(pickle_in)
		if self.kd:
			self.tree = Tree(capacity=self.maxSize*10)
			for i in range(len(self.cases)):
				case = self.getCase(i)
				self.tree.insert(case.getState(), [case.getAction(), case.getValue()])
		self.additions = self.size()

	''' Print the cases '''
	def printCases( self ):
		print("\nCases in case base:")
		for i in range(self.size()):
			case = self.getCase(i)
			print(case.toString())
		print()

if __name__ == '__main__':
	"""
	The main function called when cb.py is run
	from the command line:

	> python cb.py

	"""
	c1 = Case(0, [1,2], [1], 4)
	c2 = Case(1, [2,4], [2], 8)
	cb = CB(2, 1, kd=False)
	cb.add(c1)
	cb.add(c2)
	print("Size ", cb.size())
	cb.save()
	cb.load()	
	case, dist = cb.getNearestCase([2,3])
	print(case.toString(), " Distance: ", dist)
	cb.remove(c2)
	print("Size ", cb.size())
	case, dist = cb.getNearestCase([2,3])
	print(case.toString(), " Distance: ", dist)

	pass

	
