"""

baselineHeli.py Baseline behavior for the helicopter domain in RL Competition

"""

from baseline import Baseline

class BaselineHeli(Baseline):
	def getAction(self, state ):
		u_err  = 0 # forward velocity
		v_err  = 1 # sideways velocity
		w_err  = 2 # downward velocity
		x_err  = 3 # forward error
		y_err  = 4 # sideways error
		z_err  = 5 # downward error
		p_err  = 6 # angular rate around forward axis
		q_err  = 7 # angular rate around sideways (to the right) axis
		r_err  = 8 # angular rate around vertical (downward) axis
		qx_err = 9 # quaternion entries, x,y,z,w   q = [ sin(theta/2) * axis; cos(theta/2)]
		qy_err = 10 # where axis = axis of rotation; theta is amount of rotation around that axis
		qz_err = 11 # [recall: any rotation can be represented by a single rotation around some axis]

		weights = (0.0196, 0.7475, 0.0367, 0.0185, 0.7904, 0.0322, 0.1969, 0.0513, 0.1348, 0.02, 0.0, 0.23)

		y_w       = 0
		roll_w    = 1
		v_w       = 2
		x_w       = 3
		pitch_w   = 4
		u_w       = 5
		yaw_w     = 6
		z_w       = 7
		w_w       = 8
		ail_trim  = 9
		el_trim   = 10
		coll_trim = 11

		aileron = -weights[y_w] * state[y_err] + -weights[v_w] * state[v_err] + -weights[roll_w] * state[qx_err] + weights[ail_trim]
		elevator = -weights[x_w] * state[x_err] + -weights[u_w] * state[u_err] + weights[pitch_w] * state[qy_err] + weights[el_trim]
		rudder = -weights[yaw_w] * state[qz_err]
		coll =  weights[z_w] * state[z_err] + weights[w_w] * state[w_err] + weights[coll_trim]

		return [aileron, elevator, rudder, coll]		

	

