"""

Helicopter agent using the PI-SRL algorithm

"""

import random as rnd
import sys
from rlglue.agent.Agent import Agent
from rlglue.types import Action
from rlglue.types import Observation
from rlglue.agent import AgentLoader as AgentLoader
from rlglue.utils.TaskSpecVRLGLUE3 import TaskSpecParser

from numpy import *
from numpy.linalg import *

from srl.cb import CB
from srl.cbrimitation import CBRImitation
from srl.mccbr import MCCBR
from srl.srl import SRL
from srl.baseline import Baseline
from srl.case import Case
from srl.baselineheli import BaselineHeli
from srl.steprisk import StepRisk

class HeliAgentPI(Agent):

   
	def agent_init(self,taskSpecString):
		TaskSpec = TaskSpecParser(taskSpecString);
		if TaskSpec.valid:
			print("Task spec was valid")
		else:
			print("Task Spec could not be parsed: ", taskSpecString);
			exit()

		#parse action
		self.action = Action()
		self.int_action_ranges    = TaskSpec.getIntActions()
		self.double_action_ranges = TaskSpec.getDoubleActions()
		self.action.numInts       = len(self.int_action_ranges)
		self.action.numDoubles    = len(self.double_action_ranges)
		self.action.numChars      = TaskSpec.getCharCountActions()

		print("Loading case base...")
		self.cb = CB(12, 4, maxSize = 30000, kd=False)
		self.cb.load()	
		print("Case base: ", self.cb.size())

		self.baseline = BaselineHeli()
		self.risk = StepRisk(0.3)
		self.srlpi = SRL(self.cb, self.risk, self.baseline)

		self.step = 0
		self.episode = 0

		random.seed(0)

	def agent_start(self,observation):
		self.step+=1
		self.action.doubleArray = self.srlpi.step(observation.doubleArray, 0)
		return self.action

	def agent_step(self,reward, observation):
		self.step+=1
		self.action.doubleArray = self.srlpi.step(observation.doubleArray, reward)
		return self.action

	def agent_end(self,reward):
		self.episode+=1
		self.srlpi.computeStateValueUnknown(discount=True)
		self.srlpi.updateCases()
		self.srlpi.save()
		print("Episode: ", self.episode, " Steps: ", self.step, " Size CB: ", self.cb.size())
		self.step = 0
		

	def agent_cleanup(self):
		pass

	def agent_freeze(self):
		pass


	def agent_message(self,inMessage):
		return None


if __name__=="__main__":
	AgentLoader.loadAgent(HeliAgentPI())
